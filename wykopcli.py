#!/usr/bin/env python2

#autor: Synekdocha
#www: http://www.wykop.pl/ludzie/Synekdocha/

import hashlib
import requests
import json
import optparse
import os
import ConfigParser

login = "Synekdocha"
appkey = "jA6tucVMUszfJB7w7fQp"
apikey = "j6jwWKjy3q"
secretkey = "RZHgw0Iu5z"

class Colors:
	blue = "\033[1;34m"
	red = "\033[1;31m"
	esc = "\033[1;m"
	bold = "\033[1m"


class Wykop:
	apiurl = "http://a.wykop.pl/"
	format = "json"
	useragent = "WykopCLI"
	version = "0.1"

	def __init__(self, login = None, appkey = None, apikey = None, secretkey = None):
		self.user = login
		self.accountkey = appkey
		self.userkey = ""
		self.rank = ""
		self.apikey = apikey
		self.secretkey = secretkey
		self.req = requests.Session()
		self.req.headers.update({"User-Agent": Wykop.useragent})

	def sign(self, url, post = None):
		if post != None:
			params = ",".join(map(str, post.values()))
			urltosign = self.secretkey + url + params
		else:
			urltosign = self.secretkey + url
		return hashlib.md5(urltosign).hexdigest()


	def login(self):
		url = Wykop.apiurl + "user/login/appkey," + self.apikey
		post = {"login": self.user, "accountkey": self.accountkey}
		self.req.headers.update({"apisign": self.sign(url, post)})
		resp = self.req.post(url, data = post)
		data = resp.json()
		self.userkey = data['userkey']
		self.rank = str(data['rank'])

	def HashTagsNotificationsCount(self):
		url = Wykop.apiurl+"Mywykop/HashTagsNotificationsCount,"+"appkey,"+self.apikey+",userkey,"+self.userkey
		self.req.headers.update({"apisign": self.sign(url)})
		resp = self.req.post(url)
		data = resp.json()
		return str(data['count'])

	def NotificationsCount(self):
		url = Wykop.apiurl+"Mywykop/NotificationsCount,"+"appkey,"+self.apikey+",userkey,"+self.userkey
		self.req.headers.update({"apisign": self.sign(url)})
		resp = self.req.post(url)
		data = resp.json()
		return str(data['count'])

	def Ranking(self):
		return self.rank

	def logobig(self):
		print 
		print Colors.blue+"              ddhhyyysssssssssssssssssssssssssssyyyhhdd               "+Colors.esc
		print Colors.blue+"         hyssssssssssssssssssssssssssssssssssssssssssssssssyd         "+Colors.esc
		print Colors.blue+"      hsssssssssssssssssssssssssssssssssssssssssssssssssssssssyh      "+Colors.esc
		print Colors.blue+"    hssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssd    "+Colors.esc
		print Colors.blue+"   yssssssssssyhdd                                  ddhyssssssssssy   "+Colors.esc
		print Colors.blue+"  sssssssssh                               "+Colors.red+"hso"+Colors.blue+"            hssssssssy  "+Colors.esc
		print Colors.blue+" hsssssssy                            "+Colors.red+"dys++///o"+Colors.blue+"            dssssssssd "+Colors.esc
		print Colors.blue+" ssssssss                            "+Colors.red+"h+////////o"+Colors.blue+"            hsssssssy "+Colors.esc
		print Colors.blue+"dsssssssh                      "+Colors.red+"hos    y/////////o"+Colors.blue+"            ysssssss "+Colors.esc
		print Colors.blue+"hsssssss                   "+Colors.red+"ho++///s    s/////////o"+Colors.blue+"           dsssssss "+Colors.esc
		print Colors.blue+"ssssssss                 "+Colors.red+"s+////////y    s/////////o"+Colors.blue+"           sssssssd"+Colors.esc
		print Colors.blue+"sssssssy          "+Colors.red+"dyoh    o/////////y    s/////////o"+Colors.blue+"          sssssssh"+Colors.esc
		print Colors.blue+"sssssssy      "+Colors.red+"dyo++//+h    o/////////y    s/////////o"+Colors.blue+"         sssssssh"+Colors.esc
		print Colors.blue+"sssssssy     "+Colors.red+"o+///////+h    o/////////y    s/////////o"+Colors.blue+"        sssssssy"+Colors.esc
		print Colors.blue+"sssssssy     "+Colors.red+"d+////////+h    +/////////y    s/////////s"+Colors.blue+"       ssssssss"+Colors.esc
		print Colors.blue+"sssssssy      "+Colors.red+"d+////////+d    +/////////y    s/////////s"+Colors.blue+"      ssssssss"+Colors.esc
		print Colors.blue+"sssssssy       "+Colors.red+"d+////////+     +////////+y    s/////////s"+Colors.blue+"     ssssssss"+Colors.esc
		print Colors.blue+"sssssssy        "+Colors.red+"d+////////+     +/////////h   h////////+od"+Colors.blue+"    ssssssss"+Colors.esc
		print Colors.blue+"sssssssy         "+Colors.red+"d+////////+     +////////+so+/////+osd"+Colors.blue+"       ssssssss"+Colors.esc
		print Colors.blue+"sssssssy          "+Colors.red+"h+////////+     +////////////+osh"+Colors.blue+"           sssssssh"+Colors.esc
		print Colors.blue+"ysssssss           "+Colors.red+"h+////////+    s////////+osh"+Colors.blue+"               sssssssh"+Colors.esc
		print Colors.blue+"hsssssss            "+Colors.red+"h+////////+so+/////+oyd"+Colors.blue+"                  dsssssssd"+Colors.esc
		print Colors.blue+"dsssssssh            "+Colors.red+"h+////////////+oyd"+Colors.blue+"                      ysssssss "+Colors.esc
		print Colors.blue+" ssssssss             "+Colors.red+"y+///////+oyd"+Colors.blue+"                         dsssssssy "+Colors.esc
		print Colors.blue+" hsssssssy             "+Colors.red+"y//++oyd"+Colors.blue+"                            dssssssssd "+Colors.esc
		print Colors.blue+"  yssssssssh            "+Colors.red+"hyd"+Colors.blue+"                               hssssssssy  "+Colors.esc
		print Colors.blue+"   yssssssssssyhdd                                 dddhyssssssssssy   "+Colors.esc
		print Colors.blue+"    hssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssd    "+Colors.esc
		print Colors.blue+"      hsssssssssssssssssssssssssssssssssssssssssssssssssssssssyh      "+Colors.esc
		print Colors.blue+"         hyssssssssssssssssssssssssssssssssssssssssssssssssyd         "+Colors.esc
		print Colors.blue+"              ddhhhyyysssssssssssssssssssssssssssyyyhhdd              "+Colors.esc
		print

	def logoinfo(self):
		print
		print Colors.blue+"          hhyyysssssssssssssssssssyyyyhh          "+Colors.esc
		print Colors.blue+"     hsssssssssssssssssssssssssssssssssssssyh     "+Colors.esc
		print Colors.blue+"   ysssssssssyyyyyyyyyyyyyyyyyyyyyyyysssssssssy   "+Colors.esc
		print Colors.blue+"  ssssssy                                yssssss  "+Colors.esc
		print Colors.blue+" sssssy                    "+Colors.red+"hso///y"+Colors.blue+"         sssssy "+Colors.esc
		print Colors.blue+" sssss                 "+Colors.red+"h   s//////s"+Colors.blue+"         sssss "+Colors.esc+"   "+Colors.bold+"Login: %s" % self.user+Colors.esc
		print Colors.blue+"yssssy             "+Colors.red+"ys+//o   o//////s"+Colors.blue+"        sssss "+Colors.esc+"   "+Colors.bold+"Powiadomienia o tagach: %s" % self.HashTagsNotificationsCount()+Colors.esc
		print Colors.blue+"sssssh            "+Colors.red+"y//////o   s//////s"+Colors.blue+"       yssssh"+Colors.esc+"   "+Colors.bold+"Wiadomosci do mnie: %s" % self.NotificationsCount()+Colors.esc
		print Colors.blue+"sssssh    "+Colors.red+"hso+/o   y//////o   s//////s"+Colors.blue+"      yssssy"+Colors.esc+"   "+Colors.bold+"Pozycja w rankingu: %s" % self.Ranking()+Colors.esc
		print Colors.blue+"sssssh   "+Colors.red+"h//////+   y//////o   s//////s"+Colors.blue+"     yssssy"+Colors.esc
		print Colors.blue+"sssssh    "+Colors.red+"h+/////+   y//////o   s//////s"+Colors.blue+"    ysssss"+Colors.esc
		print Colors.blue+"sssssh     "+Colors.red+"h+/////o   y//////o   o//////s"+Colors.blue+"   ysssss"+Colors.esc
		print Colors.blue+"sssssh      "+Colors.red+"h+/////+   y//////oso+//+oy"+Colors.blue+"     ysssss"+Colors.esc
		print Colors.blue+"yssssh       "+Colors.red+"h+/////+   y///////+sh"+Colors.blue+"         yssssy"+Colors.esc
		print Colors.blue+"hssssy        "+Colors.red+"h+/////+yo+//+osh"+Colors.blue+"             sssssh"+Colors.esc
		print Colors.blue+" sssss          "+Colors.red+"+//////+oy"+Colors.blue+"                  sssss "+Colors.esc
		print Colors.blue+" sssssy          "+Colors.red+"+/+oy"+Colors.blue+"                     sssssy "+Colors.esc
		print Colors.blue+"  ssssssy                                yssssss  "+Colors.esc
		print Colors.blue+"   ysssssssssyyyyyyyyyyyyyyyyyyyyyyyysssssssssy   "+Colors.esc
		print Colors.blue+"     hsssssssssssssssssssssssssssssssssssssyh     "+Colors.esc
		print Colors.blue+"          hhyyyssssssssssssssssssssyyyhh          "+Colors.esc
		print

def main():
	wykop = Wykop(login, appkey, apikey, secretkey)
	wykop.login()
	parser = optparse.OptionParser()
	parser.add_option('-l', '--logo', action="store_true", dest="logo", help = "Duze logo Wypoku")
	parser.add_option('-i', '--info', action="store_true", dest="logoinfo", help = "Informacje o koncie")
	parser.add_option('-p','--powiadomienia',action="store_true", dest="hashnotif", help = "Ilosc powiadomien o tagach")
	parser.add_option('-w','--wiadomosci',action="store_true", dest="msgnotif", help = "Ilosc wiadomosci do mnie")
	parser.add_option('-r','--ranking',action="store_true", dest="rank", help = "Pozycja w rankingu")
	(opts, params) = parser.parse_args()

	if opts.logo:
		wykop.logobig()
	if opts.logoinfo:
		wykop.logoinfo()
	if opts.hashnotif:
		print wykop.HashTagsNotificationsCount()
	if opts.msgnotif:
		print wykop.NotificationsCount()
	if opts.rank:
		print wykop.Ranking()

if __name__ == "__main__":
	main()